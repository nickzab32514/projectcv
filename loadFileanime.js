// function loadAllStudents() {
//     var responseData = null;
//     var data = fetch('https://api.jikan.moe/v3/search/anime?q=dragonball')
//         .then((response) => {
//             console.log(response)
//             return response.json()
//         }).then((filmDetail) => {
//             responseData = filmDetail
//             var resultElement = document.getElementById('result')
//             resultElement.innerHTML = JSON.stringify(filmDetail, null, 6)
//         })
// }

async function loadAllStudentAsync() {
    let response = await fetch('https://api.jikan.moe/v3/search/anime?q=dragonball')
    let data = await response.json()
    // var resultElement = document.getElementById('result')
    // resultElement.innerHTML = JSON.stringify(data, null, 6)
    // var dataResult= data[0][result]
    return data
}

function createResultTable(data) {
    console.log(data)
    let resultElement = document.getElementById('resultTable')
    let tableNode = document.createElement('table')
    resultElement.appendChild(tableNode)
    tableNode.setAttribute('class', 'table')

    let tableHeadNode = document.createElement('thead')
    tableNode.appendChild(tableHeadNode)
    var tableRawNode = document.createElement('tr')
    tableHeadNode.appendChild(tableRawNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = '#'
    tableHeadNode.appendChild(tableHeaderNode)

    
    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'mad_id'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'title'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'image'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'synopsis'
    tableHeadNode.appendChild(tableHeaderNode)
    

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'type'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'start-date'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'End-date'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'rated'
    tableHeadNode.appendChild(tableHeaderNode)

    tableHeaderNode = document.createElement('th')
    tableHeaderNode.setAttribute('scope', 'col')
    tableHeaderNode.innerText = 'score'
    tableHeadNode.appendChild(tableHeaderNode)


    console.log(data)

    data.then((filmDetail) => {
        console.log(filmDetail)
        // var currentData = filmDetail['results'][3]
    //    var  currentData=filmDetail['results'][4]    
    
        for (let i = 0; i < filmDetail['results'].length; i++) {
            var currentData = filmDetail['results'][i]
            console.log(currentData)
            var dataRow = document.createElement('tr')
            tableNode.appendChild(dataRow)
            var dataFirstColumnNode = document.createElement('th')
            dataFirstColumnNode.setAttribute('scope', 'row')
            dataFirstColumnNode.innerText = 1+1
            dataRow.appendChild(dataFirstColumnNode)

            var columeNode = null;

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['mal_id']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['title']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            var imageNode = document.createElement('img')
            imageNode.setAttribute('src', currentData['image_url'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            dataRow.appendChild(imageNode)


            columeNode = document.createElement('td')
            columeNode.innerText = currentData['synopsis']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['type']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['start_date']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['end_date']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['score']
            dataRow.appendChild(columeNode)

            columeNode = document.createElement('td')
            columeNode.innerText = currentData['rated']
            dataRow.appendChild(columeNode)
        }
    })
}

async function loadmove() {
    console.log('test')
    let movieId = document.getElementById('queryId').value
    console.log(movieId)
    if (movieId != '' && movieId != null) {
        console.log("pass")
        let response = await fetch('https://api.jikan.moe/v3/search/anime?q=' + movieId)
        let data = await response.json()
        return data;
    }
}
function createResultSearchTable(data) {
    let resultfavorite = document.getElementById('favorite')
    resultfavorite.innerHTML=""
    let resultElementTable = document.getElementById('serach')
    let checkResultClear = document.getElementById('idDiv')

    let divResult = document.createElement('div')
    divResult.setAttribute('id','idDiv')
    divResult.setAttribute('style','text-align: start;position:center;')
    divResult.setAttribute('class','card;')
    resultElementTable.appendChild(divResult)
    // resultElementTable.innerHTML=""

    if (checkResultClear != null) {
        resultElementTable.removeChild(checkResultClear)
    }
    data.then(filmDetail => {
        console.log(filmDetail);
        for(let i=0;i<filmDetail["results"].length;i++){
            let showDetail=filmDetail["results"][i]
            let div = document.createElement('div')
            divResult.appendChild(div)
            let tr = document.createElement('tr')
            div.appendChild(tr)
            let tr2 = document.createElement('tr')
            div.appendChild(tr2)
            let th = document.createElement('th')
            tr.appendChild(th)
            let th22 = document.createElement('tr')
            tr.appendChild(th22)
            let th33 = document.createElement('tr')
            tr.appendChild(th33)
            let th44 = document.createElement('tr')
            tr.appendChild(th44)
            let th2 = document.createElement('th')
            tr2.appendChild(th2)
            div.setAttribute('class','card-body')
            // let profile = document.createElement('p')
            // profile.innerHTML = 'Anime'
            // profile.setAttribute('style', 'text-align: center;font-weight: bold;')
            // div.appendChild(profile)
            let name = document.createElement('p')
            name.setAttribute('id', 'title')
            name.setAttribute('style', 'margin-left:10px')
            name.innerText = 'ชื่อหนัง---' + showDetail['title']
            th22.appendChild(name)
            let imageNode = document.createElement('img')
            imageNode.setAttribute('src', showDetail['image_url'])
            imageNode.style.width = '200px'
            imageNode.style.height = '200px'
            th.appendChild(imageNode)

            let rate = document.createElement('p')
            rate.setAttribute('id', 'rated')
            rate.setAttribute('style', 'margin-left:10px')
            rate.innerText = 'rate---' + showDetail['rated']
            th22.appendChild(rate)
    
            let synopsis = document.createElement('p')
            synopsis.innerText = 'เรื่องย่อ--- '+ showDetail['synopsis']
            synopsis.setAttribute('style', 'margin-left:10px')
            th33.appendChild(synopsis)
            let hr = document.createElement('hr')
            divResult.appendChild(hr)
        }
    })

}
